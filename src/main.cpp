#include <iostream>
#include <fstream>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "include/ocv_versioninfo.h"

int main( int argc, char* argv[] )
{
    print_ocv_version();
    return 0;
}


